from django.forms import ModelForm
from projects.models import Project

# from tasks.models import Task


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
